# Apple ICNS Icon Generator
This simple terminal-based program automatically generates proper Apple .ICNS icon files from a single 1024x1024 px .PNG file. It makes it very easy for anyone to generate Apple icons without using Xcode or other programs. After spending two hours and three headaches trying to figure out Apple's crappy icon system, I figured I'd write a program that does it faster for anyone in the future.

## Usage
To use this program, enter the program folder with Terminal run `icnsgen "/path/to/your-icon.png" "/path/to/export.iconset"`. You will need a 1024x1024 px source image and you will need the `.iconset` at the end of your path. If the export folder doesn't exist, it will create it for you and if you forget to add this text, it will handle that as well. If an export directory isn't specified, it will simply export to whatever folder you're currently in.

## Examples
The `Example Assets` folder contains a [GIMP](https://gimp.org) .XCF file that has a template for creating a standard macOS icon. It has a drop shadow layer which is a subtle feature of Apple-made icons. It also has the correct icon shape and size. For more information about icons and macOS design guidelines, see [Apple Human Interface Guidelines - App Icon](https://developer.apple.com/design/human-interface-guidelines/macos/icons-and-images/app-icon/). There is also an example `.iconset` folder and .PNG icon to test the conversion process with. Simply enter the folder and run the program on the .PNG image and select the folder to see how it works.

## Further reading
Need more information about the ICNS file format? These sources helped me:
* [Human Interface Guidelines: App Icon - Apple Developer Docs](https://developer.apple.com/design/human-interface-guidelines/macos/icons-and-images/app-icon/)
* [Creating an App Icon - Eon's Swift Blog](https://eon.codes/blog/2016/12/06/Creating-an-app-icon/)
* [How to Manually Create ICNS Files Using Iconutil? - StackOverflow](https://stackoverflow.com/questions/12306223/how-to-manually-create-icns-files-using-iconutil)
* [Apple Icon Image Format - Wikipedia](https://en.wikipedia.org/wiki/Apple_Icon_Image_format)
